const express = require('express');
const router = express.Router();
const db = require('../fileDb');

router.get('/', (req, res) => {
    const messages = db.getItems();
    const date = req.query.datetime;

    if (date) {
        if (isNaN(new Date(date).getDate())) {
            return res.status(400).send({error: 'Date is incorrect'})
        } else {
            let lastMes = [];
            messages.forEach(mes => {
                if (date < mes.datetime) {
                    lastMes.push(mes);
                }
            });

            return res.send(lastMes);
        }
    }
    return res.send(messages);
});



router.post('/', async (req, res, next) => {
    try {
        if (!req.body.message || !req.body.author) {
            return res.status(400).send({error: 'Author and message must be present in the request'});
        }

        const message = {
            message: req.body.message,
            author: req.body.author,
        }

        await db.addItem(message);

        return res.send({message: 'Created new message with ID=' + message.id});
    } catch (e) {
        next(e);
    }
});

module.exports = router;
