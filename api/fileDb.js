const fs = require('fs').promises;
const {nanoid} = require("nanoid");

const filename = './db.json';

let data = [];

module.exports = {
    async init() {
        try {
            const fileContents = await fs.readFile(filename);
            data = JSON.parse(fileContents.toString());

        } catch (e) {
            data = [];
        }
    },
    getItems() {
        if (data.length < 30) {
            return data;
        }
        return data.slice((data.length - 30), data.length);
    },

    addItem(item) {
        item.id = nanoid();
        item.datetime = new Date().toISOString();
        data.push(item);
        return this.save();
    },
    save() {
        return  fs.writeFile(filename, JSON.stringify(data, null, 2));
    }
};
