import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MessagesComponent } from './pages/messages/messages.component';
import { EditMessageComponent } from './pages/edit-message/edit-message.component';

const routes: Routes = [
  {path: '', component: MessagesComponent},
  {path: 'messages/new', component: EditMessageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
