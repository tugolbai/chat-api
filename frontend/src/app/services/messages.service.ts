import { Injectable } from '@angular/core';
import { Message, MessageData } from '../models/message.model';
import { map, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  messagesChange = new Subject<Message[]>();
  messagesFetching = new Subject<boolean>();
  messageUploading = new Subject<boolean>();

  private messages: Message[] = [];
  interval: any = null;

  constructor(private http: HttpClient) { }

  start() {
    this.messagesFetching.next(true);
    this.http.get<[Message]>(environment.apiUrl + '/messages')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return result.map(messageData => {
          return new Message(messageData.id, messageData.message, messageData.author, messageData.datetime);
        });
      }))
      .subscribe(messages => {
        this.messages = messages;
        this.messagesChange.next(this.messages.slice());
        this.messagesFetching.next(false);
      }, () => {
        this.messagesFetching.next(false);
      });
  }


  checkNewMessage() {
    this.interval = setInterval ( () => {
      let chekUrl = environment.apiUrl + `/messages?datetime=` + this.messages[(this.messages.length - 1)].datetime;
      return  this.http.get<Message[]>(chekUrl).pipe(
        map(response => {
          return response.map(messageData => {
            return new Message(messageData.id, messageData.message, messageData.author, messageData.datetime);
          });
        })
      ).subscribe(mes => {
          if (mes === []) {
            return;
          }
          this.start();
        }
      )
    }, 10000);

  }

  stop() {
    clearInterval(this.interval);
  }

  createMessage(messageData: MessageData) {
    this.messageUploading.next(true);
    return this.http.post(environment.apiUrl + '/messages', messageData).pipe(
      tap(() => {
        this.messageUploading.next(false);
      }, () => {
        this.messageUploading.next(false);
      })
    );
  }
}
