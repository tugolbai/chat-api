import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MessageData } from '../../models/message.model';
import { Router } from '@angular/router';
import { MessagesService } from '../../services/messages.service';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-edit-message',
  templateUrl: './edit-message.component.html',
  styleUrls: ['./edit-message.component.sass']
})
export class EditMessageComponent implements OnInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  isUploading = false;
  messageUploadingSubscription!: Subscription;

  constructor(
    private messagesService: MessagesService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.messageUploadingSubscription = this.messagesService.messageUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });
  }

  onSubmit() {
    const value: MessageData = this.form.value;
    this.messagesService.createMessage(value).subscribe(() => {
      void this.router.navigate(['/']);
    });
  }

  ngOnDestroy(): void {
    this.messageUploadingSubscription.unsubscribe();
  }
}
