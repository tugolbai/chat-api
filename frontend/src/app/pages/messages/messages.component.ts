import { Component, OnDestroy, OnInit } from '@angular/core';
import { Message } from '../../models/message.model';
import { MessagesService } from '../../services/messages.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.sass']
})
export class MessagesComponent implements OnInit, OnDestroy {
  messages: Message[] = [];
  messagesChangeSubscription!: Subscription;
  messagesFetchingSubscription!: Subscription;
  isFetching = false;

  constructor(private messageService: MessagesService) { }

  ngOnInit(): void {
    this.messagesChangeSubscription = this.messageService.messagesChange.subscribe((messages: Message[]) => {
      this.messages = messages;
    });
    this.messagesFetchingSubscription = this.messageService.messagesFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.messageService.start();
    this.messageService.checkNewMessage();
  }

  ngOnDestroy() {
    this.messageService.stop();
    this.messagesChangeSubscription.unsubscribe();
    this.messagesFetchingSubscription.unsubscribe();
  }
}
